package com.q_rim.app6;

import android.support.v4.app.Fragment;

/*
 - Containing class
 */

public class PhotoPageActivity extends SingleFragmentActivity {
  @Override
  public Fragment createFragment() {
    return new PhotoPageFragment();
  }
}