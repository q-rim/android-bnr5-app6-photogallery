package com.q_rim.app6;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;

//public class PhotoGalleryFragment extends VisibleFragment {   // seems to give errors.  <30.7>
public class PhotoGalleryFragment extends Fragment {
  private static final String TAG = "---PhotoGalleryFragment";
  GridView gridView;
  ArrayList<GalleryItem> items;
  ThumbnailDownloader<ImageView> thumbnailThread;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setRetainInstance(true);
    setHasOptionsMenu(true);                  // Options menu callbacks <Listing 28.5>

    updateItems();

    this.thumbnailThread = new ThumbnailDownloader<ImageView>(new Handler());
    this.thumbnailThread.setListener(new ThumbnailDownloader.Listener<ImageView>() {
      public void onThumbnailDownloaded(ImageView imageView, Bitmap thumbnail) {
        if (isVisible()) {
          imageView.setImageBitmap(thumbnail);
        }
      }
    });
    this.thumbnailThread.start();
    this.thumbnailThread.getLooper();
    Log.i(TAG, "Background thread started");

  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_photo_gallery, container, false);
    this.gridView = (GridView)v.findViewById(R.id.gridView);
    setupAdapter();

    this.gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> gridView, View view, int position, long id) {
        GalleryItem item = items.get(position);
        Uri photoPageUri = Uri.parse(item.getPhotoPageUrl());
        Log.i(TAG, photoPageUri.toString());
        // Intent i = new Intent(Intent.ACTION_VIEW, photoPageUri);
        Intent i = new Intent(getActivity(), PhotoPageActivity.class);
        i.setData(photoPageUri);
        startActivity(i);
      }
    });
    return v;
  }

  // Options menu callbacks <Listing 28.5>
  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.fragment_photo_gallery, menu);
  }

  // Options menu callbacks <Listing 28.5>
  @Override
  @TargetApi(11)
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_item_search:
        Log.i(TAG, "Search selected");
        getActivity().onSearchRequested();
        return true;
      case R.id.menu_item_clear:
        Log.i(TAG, "Clear selected");
        PreferenceManager.getDefaultSharedPreferences(getActivity())
          .edit()
          .putString(FlickrFetchr.PREF_SEARCH_QUERY, null)
          .commit();
        updateItems();
        return true;
      case R.id.menu_item_toggle_polling:
        Log.i(TAG, "Polling selected");
        boolean shouldStartAlarm = !PollService.isServiceAlarmOn(getActivity());
        PollService.setServiceAlarm(getActivity(), shouldStartAlarm);

        // Invalidate your options menu <Listing 29.15>
        // not really needed as our min.SDK is set for > HONEYCOMB
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
          getActivity().invalidateOptionsMenu();

        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  // 1. check to see if the alarm is on.
  // 2. change text menu to show appropriate feedback <Listing 29.14>
  @Override
  public void onPrepareOptionsMenu(Menu menu) {
    super.onPrepareOptionsMenu(menu);

    MenuItem toggleItem = menu.findItem(R.id.menu_item_toggle_polling);
    // check to see if the alarm (background poll service) is on.
    if (PollService.isServiceAlarmOn(getActivity())) {
      toggleItem.setTitle(R.string.stop_polling);
    } else {
      toggleItem.setTitle(R.string.start_polling);
    }
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    this.thumbnailThread.quit();
    Log.i(TAG, "Background thread destroyed");
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    this.thumbnailThread.clearQueue();
  }

  public void updateItems() {
    new FetchItemsTask().execute();           // Executing an AsyncTask
  }

  void setupAdapter() {
    if (getActivity() == null || this.gridView == null)
      return;
    if (this.items != null) {
      this.gridView.setAdapter(new GalleryItemAdapter(this.items));
    } else {
      this.gridView.setAdapter(null);
    }
  }

  public class FetchItemsTask extends AsyncTask<Void, Void, ArrayList<GalleryItem>> {     // AsyncTask - background Task
    @Override
    protected ArrayList<GalleryItem> doInBackground(Void... params) {    // to get data from website & Log it.

      Activity activity = getActivity();
      if (activity == null)
        return  new ArrayList<GalleryItem>();

      String query = PreferenceManager.getDefaultSharedPreferences(activity)
        .getString(FlickrFetchr.PREF_SEARCH_QUERY, null);

      if (query != null) {
        Log.i(TAG, "FlickrFetcher().search(query)");
        return new FlickrFetchr().search(query);
      } else {
        Log.i(TAG, "FlickrFetcher().fetchItems()");
        return new FlickrFetchr().fetchItems();      // Getting the API response from Flickr
      }
    }

    @Override
    protected void onPostExecute(ArrayList<GalleryItem> items_FetchItemsTask) {
      items = items_FetchItemsTask;
      setupAdapter();
    }
  }

  private class GalleryItemAdapter extends ArrayAdapter<GalleryItem> {
    public GalleryItemAdapter(ArrayList<GalleryItem> items) {
      super(getActivity(), 0, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      if (convertView == null) {
        convertView = getActivity().getLayoutInflater().inflate(R.layout.gallery_item, parent, false);
      }
      ImageView imageView = (ImageView)convertView.findViewById(R.id.gallery_item_imageView);
      imageView.setImageResource(R.drawable.brian_up_close);
      GalleryItem item = getItem(position);
      thumbnailThread.queueThumbnail(imageView, item.getUrl());

      return convertView;
    }
  }
}