package com.q_rim.app6;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.ArrayList;

public class PollService extends IntentService {
  private static final String TAG = "Q---PollService";
  private static final int POLL_INTERVAL = 1000 * 1;     // 1 sec poll interval
  public static final String PREF_IS_ALARM_ON = "isAlarmOn";
  public static final String ACTION_SHOW_NOTIFICATION = "com.q_rim.app6.SHOW_NOTIFICATION";
  public static final String PERM_PRIVATE = "com.q_rim.app6.PRIVATE";   // custom permission

  public PollService() {
    super(TAG);
  }

  @Override
  protected void onHandleIntent(Intent intent) {
    Log.i(TAG, "Received an intent: " + intent);

    // checking network connectivity
    ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
    @SuppressWarnings("depreciation")
      boolean isNetworkAvailable = cm.getBackgroundDataSetting() && cm.getActiveNetworkInfo() != null;
    Log.i(TAG, "Network is UP (Background Service): " + isNetworkAvailable);
    if (!isNetworkAvailable) return;

    // --- Checking for new results <Listing 29.7> --- //
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    String query = prefs.getString(FlickrFetchr.PREF_SEARCH_QUERY, null);
    String lastResultId = prefs.getString(FlickrFetchr.PREF_LAST_RESULT_ID, null);

    // 1. pull out current query and latest result ID from prefs
    // 2. Fetch the latest result set with FlickrFetcher.
    ArrayList<GalleryItem> items;
    if (query != null) {
      items = new FlickrFetchr().search(query);
    } else {
      items = new FlickrFetchr().fetchItems();
    }

    // 3. If there are results, grab the first one
    if (items.size() == 0) return;

    String resultId = items.get(0).getId();

    // 4. Check to see if it is different from the last result ID.
    if (!resultId.equals(lastResultId)) {
      Log.i(TAG, "NEW result: " + resultId);

      // Notification <Listing 29.16>
      Resources r = getResources();
      PendingIntent pi = PendingIntent.getActivity(this, 0, new Intent(this, PhotoGalleryActivity.class), 0);
      Notification notification = new NotificationCompat.Builder(this)
        .setTicker(r.getString(R.string.new_pictures_title))              // configure ticker text
        .setSmallIcon(android.R.drawable.ic_menu_report_image)            // configure small icon
        .setContentTitle(r.getString(R.string.new_pictures_title))        // set title
        .setContentText(r.getString(R.string.new_pictures_text))          // set text
        .setContentIntent(pi)
        .setAutoCancel(true)          // notification is deleted once the user checks the notification.
        .build();

      // Using the custom method instead of NotificationManager to post notification.
      showBackgroundNotification(0, notification);
//      NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
//      notificationManager.notify(0, notification);      // 0 - identifier for the notification;
//      // Broadcast Intent Send <Listing 30.5>
//      sendBroadcast(new Intent(ACTION_SHOW_NOTIFICATION), PERM_PRIVATE);

    } else {
      Log.i(TAG, "OLD result: " + resultId);
    }

    // 5. Store the first result back in prefs.
    prefs.edit()
      .putString(FlickrFetchr.PREF_LAST_RESULT_ID, resultId)
      .commit();
  }

  // Add alarm method <Listing 29.8>
  // keep running polling in the background at 5 sec interval
  public static void setServiceAlarm(Context context, boolean isOn) {
    Intent i = new Intent(context, PollService.class);
    // construct your PendingIntent that starts PollService.
    PendingIntent pi = PendingIntent.getService(context, 0, i, 0);

    AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

    // set the alarm or cancel it
    if (isOn) {
      Log.i(TAG, "Setting Alarm @ interval: " + POLL_INTERVAL);
      alarmManager.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), POLL_INTERVAL, pi);
    } else {
      Log.i(TAG, "Canceling Alarm!!!");
      alarmManager.cancel(pi);
      pi.cancel();
    }

    // Add alarm status preference <Listing 30.3>
    // Resetting the recurring alarm upon system reboot through Broadcast Receiver.
    PreferenceManager.getDefaultSharedPreferences(context)
      .edit()
      .putBoolean(PollService.PREF_IS_ALARM_ON, isOn)
      .commit();
  }

  // Checks if the Alarm is ON/OFF
  public static boolean isServiceAlarmOn(Context context) {
    Intent i = new Intent(context, PollService.class);
    PendingIntent pi = PendingIntent.getService(context, 0, i, PendingIntent.FLAG_NO_CREATE);
    return pi != null;
  }

  // Sending Ordered Broadcast <30.12>
  void showBackgroundNotification(int requestCode, Notification notification) {
    Intent i = new Intent(ACTION_SHOW_NOTIFICATION);
    i.putExtra("REQEST_CODE", requestCode);
    i.putExtra("NOTIFICATION", notification);

    sendOrderedBroadcast(i, PERM_PRIVATE, null, null,
                      Activity.RESULT_OK, null, null);
  }
}
