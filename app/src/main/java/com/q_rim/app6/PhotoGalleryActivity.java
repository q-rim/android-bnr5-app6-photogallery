package com.q_rim.app6;


import android.app.SearchManager;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;

public class PhotoGalleryActivity extends SingleFragmentActivity {
  private static final String TAG = "---PhotoGalleryActivity";

  @Override
  public Fragment createFragment() {
    return new PhotoGalleryFragment();
  }

// Override onNewIntent <Listing 28.9>
  @Override
  public void onNewIntent(Intent intent) {
    PhotoGalleryFragment fragment = (PhotoGalleryFragment)getSupportFragmentManager()
      .findFragmentById(R.id.fragmentContainer_FrameLayout);

    if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
      String query = intent.getStringExtra(SearchManager.QUERY);
      Log.i(TAG, "Received a new search query: " + query);

      // Save out search query <Listing 28.11>
      PreferenceManager.getDefaultSharedPreferences(this)
        .edit()
        .putString(FlickrFetchr.PREF_SEARCH_QUERY, query)
        .commit();
    }
    fragment.updateItems();
  }
}
