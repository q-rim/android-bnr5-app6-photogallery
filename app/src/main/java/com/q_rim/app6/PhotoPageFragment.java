package com.q_rim.app6;

/*
Fragment for Flickr WebView:
- Inflate layout file
- extract WebView
- Forward along URL from intent's data
 */

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

public class PhotoPageFragment extends VisibleFragment {
  private String url;
  private WebView webView;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setRetainInstance(true);

    this.url = getActivity().getIntent().getData().toString();
    Log.i("Q--- PhotoPageFragment", "url=" + url);
  }

  @SuppressLint("SetJavaScriptEnabled")
  @Override     // inflate
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_photo_page, parent, false);

    // Wiring up ProgressBar <31.9>
    final ProgressBar progressBar = (ProgressBar)v.findViewById(R.id.progressBar);
    progressBar.setMax(100);    // WebChromeClient reports in range 0-100
    final TextView titleTextView = (TextView)v.findViewById(R.id.title_TextView);

    this.webView = (WebView)v.findViewById(R.id.webView);
    this.webView.getSettings().setJavaScriptEnabled(true);      // turn on JavaScript
    this.webView.setWebViewClient(new WebViewClient() {
      @Override     // prevent default implimentation of being forwarded to web browser.  Although this wasn't the case  <p.508>
      public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return false;
      }
    });

    // ProgressBar <31.9>
    this.webView.setWebChromeClient(new WebChromeClient() {
      public void onProgressChanged(WebView webView, int progress) {
        if (progress == 100) {
          progressBar.setVisibility(View.INVISIBLE);
        } else {
          progressBar.setVisibility(View.VISIBLE);
          progressBar.setProgress(progress);
        }
      }
      public void onReceivedTitle(WebView webView, String title) {
        titleTextView.setText(title);
      }
    });

    this.webView.loadUrl(this.url);   // loading url has to be done after configuring WebView.
    return v;
  }
}
