package com.q_rim.app6;

import android.net.Uri;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/*
HTTP Networking
This code creates URL object from a String. ex: http://www.google.com
Then it calls openConnection() to create a connection object pointed at the URL.
URL.openConnection() returns a URLConnection, but since you are connecting to http URL, you can cast it to HttpUrlConnection.
This gives you HTTP-specific interfaces for working with request methods, response codes, streaming methods, and more.
 */

public class FlickrFetchr {
  public static final String TAG = "---FlickrFetchr";
  public static final String PREF_SEARCH_QUERY = "searchQuery";
  public static final String PREF_LAST_RESULT_ID = "lastResultId";

  // API request
  // https://api.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=c55abd9ab9c95bd9fa5aa55b19913d88
  private static final String ENDPOINT = "https://api.flickr.com/services/rest/";
  private static final String API_KEY = "c55abd9ab9c95bd9fa5aa55b19913d88";
  private static final String METHOD_GET_RECENT = "flickr.photos.getRecent";
  private static final String METHOD_SEARCH = "flickr.photos.search";
  private static final String PARAM_EXTRAS = "extras";
  private static final String EXTRA_SMALL_URL = "url_s";
  private static final String XML_PHOTO = "photo";
  private static final String PARAM_TEXT = "text";

  byte[] getUrlBytes(String urlSpec) throws IOException {
    URL url = new URL(urlSpec);
    HttpURLConnection connection = (HttpURLConnection)url.openConnection();     // represents a http connection

    try {
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      InputStream in = connection.getInputStream();                         // initiate http connection

      if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
        return null;
      }

      int bytesRead = 0;
      byte[] buffer = new byte[1024];
      while ((bytesRead = in.read(buffer)) > 0) {     // continuously call "read" until the connection runs out of data
        out.write(buffer, 0, bytesRead);
      }
      out.close();                                    // make sure to close the connection.
      return out.toByteArray();
    } finally {
      connection.disconnect();
    }
  }

  public String getUrl(String urlSpec) throws IOException {
    return new String(getUrlBytes(urlSpec));        // convert Bytes into String
  }

  // Putting together the API request
  public ArrayList<GalleryItem> downloadGalleryItems(String url) {
    ArrayList<GalleryItem> items = new ArrayList<GalleryItem>();

    try {
      String xmlString = getUrl(url);
      Log.i(TAG, "*** Tx ***: " + url);
      Log.i(TAG, "*** Rx ***: " + xmlString);
      XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
      XmlPullParser parser = factory.newPullParser();
      parser.setInput(new StringReader(xmlString));

      parseItems(items, parser);
    } catch (IOException ioe) {
      Log.e(TAG, "Failed to fetch items", ioe);
    } catch (XmlPullParserException xppe) {
      Log.e(TAG, "Failed to parse items", xppe);
    }
    return items;
  }

  public ArrayList<GalleryItem> fetchItems() {
    String url = Uri.parse(ENDPOINT).buildUpon()
      .appendQueryParameter("method", METHOD_GET_RECENT)
      .appendQueryParameter("api_key", API_KEY)
      .appendQueryParameter(PARAM_EXTRAS, EXTRA_SMALL_URL)
      .build().toString();
    return downloadGalleryItems(url);
  }

  public ArrayList<GalleryItem> search(String query) {
    String url = Uri.parse(ENDPOINT).buildUpon()
      .appendQueryParameter("method", METHOD_SEARCH)
      .appendQueryParameter("api_key", API_KEY)
      .appendQueryParameter(PARAM_EXTRAS, EXTRA_SMALL_URL)
      .appendQueryParameter(PARAM_TEXT, query)
      .build().toString();
    return downloadGalleryItems(url);
  }

  void parseItems(ArrayList<GalleryItem> items, XmlPullParser parser) throws XmlPullParserException, IOException {
    int eventType = parser.next();

    while (eventType != XmlPullParser.END_DOCUMENT) {
      if (eventType == XmlPullParser.START_TAG && XML_PHOTO.equals(parser.getName())) {
        String id = parser.getAttributeValue(null, "id");
        String caption = parser.getAttributeValue(null, "title");
        String smallUrl = parser.getAttributeValue(null, EXTRA_SMALL_URL);
        String owner = parser.getAttributeValue(null, "owner");             // getting the photo owner from XML <31.2>

        GalleryItem item = new GalleryItem();
        item.setId(id);
        item.setCaption(caption);
        item.setUrl(smallUrl);
        item.setOwner(owner);
        items.add(item);
      }
      eventType = parser.next();
    }
  }
}
