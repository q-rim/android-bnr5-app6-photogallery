package com.q_rim.app6;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;

import java.util.ArrayList;

public class PhotoGalleryFragment extends Fragment {
  private static final String TAG = "---PhotoGalleryFragment";
  GridView gridView;
  ArrayList<GalleryItem> items;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setRetainInstance(true);
    new FetchItemsTask().execute();           // Executing an AsyncTask
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_photo_gallery, container, false);
    this.gridView = (GridView)v.findViewById(R.id.gridView);
    return v;
  }

  void setupAdapter() {
    if (getActivity() == null || this.gridView == null)
      return;
    if (this.items != null) {
      this.gridView.setAdapter(new ArrayAdapter<GalleryItem>(getActivity(), android.R.layout.simple_gallery_item, this.items));
    } else {
      this.gridView.setAdapter(null);
    }
  }

  public class FetchItemsTask extends AsyncTask<Void, Void, ArrayList<GalleryItem>> {     // AsyncTask - background Task
    @Override
    protected ArrayList<GalleryItem> doInBackground(Void... params) {    // to get data from website & Log it.

      return new FlickrFetchr().fetchItems();      // Getting the API response from Flickr
    }

    @Override
    protected void onPostExecute(ArrayList<GalleryItem> items_FetchItemsTask) {
      items = items_FetchItemsTask;
      setupAdapter();
    }
  }
}